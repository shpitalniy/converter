/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./scripts/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./scripts/helpers.js":
/*!****************************!*\
  !*** ./scripts/helpers.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const variables = __webpack_require__(/*! ./variables */ \"./scripts/variables.js\");\r\n\r\nfunction getConvertedValue(convertValue, convertFrom, convertTo){\r\n    return convertValue * variables.convertValueObj[convertFrom][convertTo];\r\n}\r\n\r\nfunction setConvertedValueInOutput(output, val){\r\n    output.value = val;\r\n}\r\n\r\nfunction showModalWindow(elem){\r\n    elem.classList.toggle('show-modal-window', true);\r\n}\r\n\r\nfunction closeModalWindow(elem){\r\n    elem.classList.toggle('show-modal-window', false);\r\n}\r\n\r\nfunction setValueInSessionStorage(key, item){\r\n    sessionStorage.setItem(key, item);\r\n}\r\n\r\nfunction checkValueInSessionStorage(key){\r\n    if(sessionStorage.hasOwnProperty(key)){\r\n        return true;\r\n    };\r\n    return false;\r\n}\r\n\r\nfunction getValueFromSessionStorage(elem){\r\n    if(checkValueInSessionStorage(elem.id)){\r\n        elem.value = sessionStorage.getItem(elem.id);\r\n    }\r\n    return false;\r\n}\r\n\r\nfunction defineLanguage(elem){\r\n    var lang = \"English\";\r\n    if(sessionStorage.hasOwnProperty(elem.id)){\r\n        lang = sessionStorage.getItem(elem.id);\r\n    }\r\n    return lang;\r\n}\r\n\r\nmodule.exports = {\r\n    getConvertedValue,\r\n    setConvertedValueInOutput,\r\n    showModalWindow,\r\n    closeModalWindow,\r\n    setValueInSessionStorage,\r\n    checkValueInSessionStorage,\r\n    getValueFromSessionStorage,\r\n    defineLanguage\r\n}\n\n//# sourceURL=webpack:///./scripts/helpers.js?");

/***/ }),

/***/ "./scripts/index.js":
/*!**************************!*\
  !*** ./scripts/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _style_style_less__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style/style.less */ \"./style/style.less\");\n/* harmony import */ var _style_style_less__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_style_less__WEBPACK_IMPORTED_MODULE_0__);\n\r\nconst variables = __webpack_require__(/*! ./variables */ \"./scripts/variables.js\");\r\nconst helpers = __webpack_require__(/*! ./helpers */ \"./scripts/helpers.js\");\r\n\r\nwindow.onload = function(){\r\n    helpers.getValueFromSessionStorage(variables.convertValue);\r\n    helpers.getValueFromSessionStorage(variables.convertFrom);\r\n    helpers.getValueFromSessionStorage(variables.convertTo);\r\n    helpers.getValueFromSessionStorage(variables.resultValue);\r\n    helpers.getValueFromSessionStorage(variables.languageSelect);\r\n    renderPage();\r\n}\r\n\r\nvariables.buttonConvert.addEventListener(\"click\", () => {\r\n    runConvert(variables.convertValue, variables.convertFrom, variables.convertTo, variables.resultValue);\r\n});\r\nvariables.buttonSettings.addEventListener(\"click\", () => {\r\n    helpers.showModalWindow(variables.modalWindow);\r\n});\r\nvariables.buttonApply.addEventListener(\"click\", () => {\r\n    helpers.setValueInSessionStorage(variables.languageSelect.id, variables.languageSelect.value);\r\n    helpers.closeModalWindow(variables.modalWindow);\r\n    renderPage();\r\n});\r\nvariables.convertValue.addEventListener(\"keyup\", () => {\r\n    helpers.setValueInSessionStorage(variables.convertValue.id, variables.convertValue.value);\r\n})\r\nvariables.convertFrom.addEventListener(\"change\", () => {\r\n    helpers.setValueInSessionStorage(variables.convertFrom.id, variables.convertFrom.value);\r\n})\r\nvariables.convertTo.addEventListener(\"change\", () => {\r\n    helpers.setValueInSessionStorage(variables.convertTo.id, variables.convertTo.value);\r\n})\r\n\r\nfunction runConvert(inputValue, inputFrom, inputTo, output){\r\n    let value = +inputValue.value;\r\n    let from = inputFrom.value;\r\n    let to = inputTo.value;\r\n\r\n    let result = helpers.getConvertedValue(value, from, to);\r\n    \r\n    helpers.setConvertedValueInOutput(output, result);\r\n    helpers.setValueInSessionStorage(output.id, output.value);\r\n}\r\n\r\nfunction renderPage(){\r\n    var lang = helpers.defineLanguage(variables.languageSelect);\r\n    if(lang != variables.currentLanguage){\r\n        for(var key in variables.elementsObj){\r\n            variables.elementsObj[key].innerHTML = variables.languageObj[lang][key]; \r\n        }\r\n        if(lang == 'Arabian'){\r\n            variables.wrapperElement.classList.toggle('arabian-direction', true);\r\n        }else{\r\n            variables.wrapperElement.classList.toggle('arabian-direction', false);  \r\n        }\r\n        variables.currentLanguage = lang;\r\n    }\r\n}\n\n//# sourceURL=webpack:///./scripts/index.js?");

/***/ }),

/***/ "./scripts/variables.js":
/*!******************************!*\
  !*** ./scripts/variables.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const convertValueObj = {\r\n    \"Versts\": {\r\n        \"Versts\": 1,\r\n        \"Meters\": 1066.8,\r\n        \"Yards\": 1166.67,\r\n        \"Foots\": 3500,\r\n    },\r\n    \"Meters\": {\r\n        \"Versts\": 0.0009,\r\n        \"Meters\": 1,\r\n        \"Yards\": 1.0936,\r\n        \"Foots\": 3.2808,\r\n    },\r\n    \"Yards\":{\r\n        \"Versts\": 0.00086,\r\n        \"Meters\": 0.9144,\r\n        \"Yards\": 1,\r\n        \"Foots\": 3,        \r\n    },\r\n    \"Foots\": {\r\n        \"Versts\": 0.00029,\r\n        \"Meters\": 0.3048,\r\n        \"Yards\": 0.3333,\r\n        \"Foots\": 1,  \r\n    }\r\n}\r\nconst currentLanguage = \"English\";\r\nconst buttonConvert = document.getElementById('buttonConvert');\r\nconst convertValue = document.getElementById('convertValue');\r\nconst convertFrom = document.getElementById('convertFrom');\r\nconst convertTo = document.getElementById('convertTo');\r\nconst resultValue = document.getElementById('resultValue');\r\nconst buttonSettings = document.getElementById('buttonSettings');\r\nconst modalWindow = document.getElementById('modalWindow');\r\nconst buttonApply = document.getElementById('buttonApply');\r\nconst languageSelect = document.getElementById('language');\r\nconst convertFromVersts = document.getElementById('convertFromVersts');\r\nconst convertFromMeters = document.getElementById('convertFromMeters');\r\nconst convertFromYards = document.getElementById('convertFromYards');\r\nconst convertFromFoots = document.getElementById('convertFromFoots');\r\nconst convertToVersts = document.getElementById('convertToVersts');\r\nconst convertToMeters = document.getElementById('convertToMeters');\r\nconst convertToYards = document.getElementById('convertToYards');\r\nconst convertToFoots = document.getElementById('convertToFoots');\r\nconst wrapperElement = document.querySelector('.wrapper');\r\nconst languageObj = {\r\n    English:{\r\n        [convertFromVersts.id]: 'Versts',\r\n        [convertFromMeters.id]: 'Meters',\r\n        [convertFromYards.id]: 'Yards',\r\n        [convertFromFoots.id]: 'Foots',\r\n        [convertToVersts.id]: 'Versts',\r\n        [convertToMeters.id]: 'Meters',\r\n        [convertToYards.id]: 'Yards',\r\n        [convertToFoots.id]: 'Foots',\r\n        [buttonConvert.id]: 'Convert',\r\n        [buttonApply.id]: 'Apply',\r\n    },\r\n    Russian:{\r\n        [convertFromVersts.id]: 'Версты',\r\n        [convertFromMeters.id]: 'Метры',\r\n        [convertFromYards.id]: 'Ярды',\r\n        [convertFromFoots.id]: 'Футы',\r\n        [convertToVersts.id]: 'Версты',\r\n        [convertToMeters.id]: 'Метры',\r\n        [convertToYards.id]: 'Ярды',\r\n        [convertToFoots.id]: 'Футы',\r\n        [buttonConvert.id]: 'Конвертировать',\r\n        [buttonApply.id]: 'Применить',        \r\n    },\r\n    Arabian:{\r\n        [convertFromVersts.id]: 'معالم',\r\n        [convertFromMeters.id]: 'متر',\r\n        [convertFromYards.id]: 'ياردة',\r\n        [convertFromFoots.id]: 'قدم',\r\n        [convertToVersts.id]: 'معالم',\r\n        [convertToMeters.id]: 'متر',\r\n        [convertToYards.id]: 'ياردة',\r\n        [convertToFoots.id]: 'قدم',  \r\n        [buttonConvert.id]: 'تحويل',\r\n        [buttonApply.id]: 'للتقديم' \r\n    },\r\n}\r\nconst elementsObj = {\r\n    [convertFromFoots.id]: convertFromFoots,\r\n    [convertFromMeters.id]: convertFromMeters,\r\n    [convertFromVersts.id]: convertFromVersts,\r\n    [convertFromYards.id]: convertFromYards,\r\n    [convertToFoots.id]: convertToFoots,\r\n    [convertToMeters.id]: convertToMeters,\r\n    [convertToVersts.id]: convertToVersts,\r\n    [convertToYards.id]: convertToYards,\r\n    [buttonConvert.id]: buttonConvert,\r\n    [buttonApply.id]: buttonApply,\r\n}\r\n\r\nmodule.exports = {\r\n    convertValueObj,\r\n    buttonConvert,\r\n    convertValue,\r\n    convertFrom,\r\n    convertTo,\r\n    resultValue,\r\n    buttonSettings,\r\n    modalWindow,\r\n    buttonApply,\r\n    languageSelect,\r\n    elementsObj,\r\n    languageObj,\r\n    currentLanguage,\r\n    wrapperElement\r\n}\n\n//# sourceURL=webpack:///./scripts/variables.js?");

/***/ }),

/***/ "./style/style.less":
/*!**************************!*\
  !*** ./style/style.less ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./style/style.less?");

/***/ })

/******/ });